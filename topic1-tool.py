import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import yfinance as yf


# Calculate the return from the prices
def calculate_returns(prices):
    returns = np.log(prices / prices.shift(1))
    returns = returns.dropna()  # Remove empty results
    return returns


# Fetch historical price data from Yahoo Finance
def get_historical_data(tickers):
    result = pd.DataFrame()
    for ticker in tickers:
        data = yf.download(ticker, period="5y", interval="1mo", progress=False)
        result[f'{ticker}_Close'] = data['Adj Close']  # Fetch adjusted close prices
    return result


# CAPM Beta Calculation
def capm_beta(data, index_col, stock_col):
    index_returns = calculate_returns(data[index_col])
    stock_returns = calculate_returns(data[stock_col])

    X = index_returns.values.reshape(-1, 1)  # Market returns (independent variable)
    Y = stock_returns.values  # Stock returns (dependent variable)

    model = LinearRegression().fit(X, Y)
    beta = model.coef_[0]  # Beta
    r_squared = model.score(X, Y)  # R² value

    return beta, r_squared, index_returns, stock_returns


# Plotting regression scatter plot
def plot_regression(index_returns, stock_returns, beta, stock_symbol):
    plt.figure(figsize=(10, 6))
    plt.scatter(index_returns, stock_returns, color='blue', label='Data Points')
    plt.plot(index_returns, beta * index_returns, color='red', label=f'Regression Line (Beta = {beta:.2f})')

    # Labels and title
    plt.title(f"CAPM Regression: {stock_symbol} vs. Market Returns (S&P 500)")
    plt.xlabel("Market Returns (Index)")
    plt.ylabel("Stock Returns")
    plt.legend()
    plt.show()


# Tool for computing and plotting CAPM-beta
def capm_tool(index_symbol, stock_symbol):
    tickers = [index_symbol, stock_symbol]
    data = get_historical_data(tickers)

    index_col = f'{index_symbol}_Close'
    stock_col = f'{stock_symbol}_Close'
    beta, r_squared, index_returns, stock_returns = capm_beta(data, index_col, stock_col)

    print(f"CAPM-Beta for {stock_symbol}: {beta:.4f}")
    print(f"R² Value: {r_squared:.4f}")

    plot_regression(index_returns, stock_returns, beta, stock_symbol)

    return beta


# Compute beta for each stock and print
def compute_all_beta():
    index_symbol = '^GSPC'
    stocks = ['F', 'TSLA', 'BKNG', 'EXPE']
    betas = {}

    for stock in stocks:
        print(f"Processing {stock}...")
        beta = capm_tool(index_symbol, stock)
        betas[stock] = beta

    return betas


# Market capitalization rate calculation
def market_capitalization_rate(beta):
    risk_free_rate = 3.7
    exp_market_return = 7

    expected_return = risk_free_rate + beta * (exp_market_return - risk_free_rate)
    return round(expected_return, 2)


# Growth rate calculation using CAPM
def compute_growth(r, div, price):
    g = r - (div / price)
    return round(g, 4)


# Present Value of Growth Opportunities (PVGO) calculation
def compute_pvgo(price, eps, rate):
    rate /= 100  # Convert rate to a percentage
    division = eps / rate
    pvgo = price - round(division, 2)
    return pvgo


# Determine if the stock is growth or value based on PVGO
def determine_stock_type(price, eps, beta):
    rate = market_capitalization_rate(beta)
    pvgo = compute_pvgo(price, eps, rate)

    if pvgo > price * 0.3:
        return "=> Growth Stock"
    else:
        return "=> Value Stock"


# Main code execution
print(""" █████╗    ██╗   ███████╗     █████╗ ███████╗███████╗██╗ ██████╗ ███╗   ██╗███╗   ███╗███████╗███╗   ██╗████████╗     ██╗
██╔══██╗   ██║   ██╔════╝    ██╔══██╗██╔════╝██╔════╝██║██╔════╝ ████╗  ██║████╗ ████║██╔════╝████╗  ██║╚══██╔══╝    ███║
███████║████████╗█████╗      ███████║███████╗███████╗██║██║  ███╗██╔██╗ ██║██╔████╔██║█████╗  ██╔██╗ ██║   ██║       ╚██║
██╔══██║██╔═██╔═╝██╔══╝      ██╔══██║╚════██║╚════██║██║██║   ██║██║╚██╗██║██║╚██╔╝██║██╔══╝  ██║╚██╗██║   ██║        ██║
██║  ██║██████║  ██║         ██║  ██║███████║███████║██║╚██████╔╝██║ ╚████║██║ ╚═╝ ██║███████╗██║ ╚████║   ██║        ██║
╚═╝  ╚═╝╚═════╝  ╚═╝         ╚═╝  ╚═╝╚══════╝╚══════╝╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚═╝     ╚═╝╚══════╝╚═╝  ╚═══╝   ╚═╝        ╚═╝

""")

if __name__ == "__main__":
    print("~~~~~~~~~~~~~~~ Computing Betas for Ford, Tesla, Priceline, and Expedia ~~~~~~~~~~~~~")
    betas = compute_all_beta()

    print("\n~~~~~~~~~~~~~~ Market Capitalization Rate ~~~~~~~~~~~~~")
    stocks_info = {
        'F': {'price': 10.2, 'eps': 0.96, 'div': 0.15},
        'TSLA': {'price': 217.06, 'eps': 3.55, 'div': 0},
        'BKNG': {'price': 3720.46, 'eps': 142.66, 'div': 0},
        'EXPE': {'price': 126.57, 'eps': 5.55, 'div': 0},
    }

    for stock, info in stocks_info.items():
        print(f"\nProcessing {stock}...")
        beta = betas[stock]
        market_rate = market_capitalization_rate(beta)
        print(f"Market Capitalization Rate for {stock}: {market_rate}%")

        # Growth Rate Calculation
        if stock == 'F':
            growth_rate = compute_growth(market_rate, info['div'], info['price'])
            print(f"Growth Rate for {stock}: {growth_rate}%")

        # PVGO and Stock Type Determination
        pvgo = compute_pvgo(info['price'], info['eps'], market_rate)
        print(f"PVGO of {stock}: ${pvgo}")
        stock_type = determine_stock_type(info['price'], info['eps'], beta)
        print(f"{stock} is classified as: {stock_type}")
