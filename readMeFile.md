**# Name**

    Finance Python Project - module 5
    Project F4E Part I Topic 1

**# Description**

The aim of Project Part I is to relate the theory of Accounting & Finance and Option Pricing to practice.

Several investment competitions are integrated in the various topics of Part I.

The most recent annual reports of some selected companies (Ford, Tesla, Priceline/Booking Holdings and
Expedia) form the standard context for most questions; however, this selection of companies generally plays 
no special role if it comes to the investment competitions.   

**# List of exercises**

a. Start with valuation by comparables, as described in Ch 4.2, for Ford and Priceline. Do you find
indications of over- or underpricing, according to this method? Briefly comment on the validity of
this method for Tesla.

b. Build a tool1 for determining the CAPM-beta, with (at least) the following specifications.

Input: the tool must be able to read a table with three columns:

     Column 1: a series of dates at a regular time interval (can be daily, weekly, monthly, e.g. as
    obtained via historical prices in yahoo.com), with the most recent date on top

     Column 2: the price of an index at each date (e.g. S&P500)

     Column 3: the price of a share at each date (e.g. Ford)

Output:

     the (CAPM-)beta of the share, with the index taken as the market portfolio.

     the corresponding R2 value.

     a scatterplot depicting the underlying regression.

c. Apply your tool to compute the beta of Ford, Tesla, Priceline, and Expedia, with S&P500 as market
portfolio, and using monthly prices over the last 5 years. Compare the outcome to reported figures
of these betas at yahoo.com, and at Investopedia. If you see substantial discrepancies of the three
betas for the same company (the one you computed, the one at yahoo.com, the one at
Investopedia), an attempt to explain this is appreciated.

d. Apply the CAPM formula to derive estimations of the market capitalization rate ( r ) of the four
companies. Next consider the following two formulas, mentioned in Ch 4.3-4, and discussed in the second lecture A&F:
    
    Price = P0 = DIV1 / (r-g) (1)
    Price = P0 = EPS1 / r + PVGO (2)

e. Use formula (1) to obtain an estimation of the growth rate g for Ford. Try to find information or
recent analyst opinions that confirm or contradict this estimation.

f. Use formula (2) to determine, for each of the four stocks, whether they are growth stocks or value
stocks. 


**# Authors**

Ruginosu Maria-Ioana -s3190285
Secara Maria - s3142388
Zvinca Daria Alexandra s3214915 
Bokyung Kim - s3102378
Martina Roci-s33096306
Begaiym Sarbagysheva - s2875233



